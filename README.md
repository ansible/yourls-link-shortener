# Yourls link shortener

Ansible role to install Yourls (Your Own URL Shortener, a PHP web app).
This role was forked from [commit 5dd4cd0](https://github.com/aabouzaid/ansible-role-yourls/commit/5dd4cd02d73ba42aa8149df575e3212f08f1c47a) of [ansible-role-yourls](https://github.com/AAbouZaid/ansible-role-yourls) by [Ahmed AbouZaid](http://tech.aabouzaid.com),
whom I gratefully acknowledge for having freely shared his work.

This role assumes the host running Yourls uses Apache web server.
If that host should serve the app over HTTPS (TLS), you should set
`yourls.www.enable_tls: true` and `yourls.www.tls_proxy: false`
(which is the role's default).
But If that host sits behind a TLS-terminating proxy, you should set the opposite,
i.e., `yourls.www.enable_tls: false` and `yourls.www.tls_proxy: true`.


## First-time setup

After setting up Yourls with this role, visit `http://<your-ls-domain>/admin`
and click the **Install Yourls** button. This makes Yourls populate the database
and complete some other stuff, allowing you to login to the admin area and start
using Yourls.

You might want to enable the `Random ShortURLs` plugin (it is a core plugin,
already installed by default), otherwise the keyword part of the short URLs
are numeric and incremental (`1`, `2`, etc.).

+ https://github.com/YOURLS/YOURLS/issues/2569
+ https://github.com/suryatanjung/yourls-limit-custom-keyword-length



## Plugins and integrations

Lots of plugins and integrations, some might be interesting.

+ https://github.com/YOURLS/awesome
+ https://github.com/miwasp/yourls-shaarli
+ https://github.com/jonrandoem/yourls-wallabag



## Example playbook

In `group_vars` or `host_vars` or similar, set `yourls` dict (note that you only
need to specify the variables you wish to override from the role's defaults):
```yaml
yourls:
  www:
     domain: "<your-ls-domain>"
     enable_tls: false
     tls_proxy: true
  db:
    user: "yourls"
    pass: "secretpassword"
  users:
    myuser: "secretpassword"
```

Playbook including necessary dependency roles:
```yaml
- name: yourls link shortener
  hosts: pergamon
  roles:
    # https://codeberg.org/ansible/apache
    - { role: apache, become: true, tags: apache }
    # https://codeberg.org/ansible/mariadb
    - { role: mariadb, become: true, tags: mariadb }
    # https://codeberg.org/ansible/php-versions
    - { role: php-versions, become: true, tags: php }
    # https://codeberg.org/ansible/php
    - { role: php, become: true, tags: php }
    # https://codeberg.org/ansible/yourls-link-shortener
    - { role: yourls-link-shortener, become: true, tags: yourls }
```

On the TLS-terminating proxy (if any), the following Apache virtualhost config should
be enough:
```
<VirtualHost *:80>
   ServerName {{ yourls.www.domain }}
   Redirect permanent / https://{{ yourls.www.domain }}/
   ErrorLog ${APACHE_LOG_DIR}/{{ yourls.www.domain }}_error.log
   CustomLog ${APACHE_LOG_DIR}/{{ yourls.www.domain }}_access.log combined
</VirtualHost>
<VirtualHost *:443>
   ServerName {{ yourls.www.domain }}
   ProxyRequests Off
   ProxyPreserveHost On
   ProxyPass / http://<ipaddr-proxy>/
   ProxyPassReverse / http://<ipaddr-proxy>/
   SSLEngine on
   SSLCertificateKeyFile   /etc/letsencrypt/live/{{ yourls.www.domain }}/privkey.pem
   SSLCertificateFile      /etc/letsencrypt/live/{{ yourls.www.domain }}/cert.pem
   SSLCertificateChainFile /etc/letsencrypt/live/{{ yourls.www.domain }}/chain.pem
   ErrorLog ${APACHE_LOG_DIR}/{{ yourls.www.domain }}_error.log
   CustomLog ${APACHE_LOG_DIR}/{{ yourls.www.domain }}_access.log combined
</VirtualHost>
```



## License

GNU GPL v3.


## Links and notes

+ https://yourls.org
+ https://yourls.org/docs/guide/advanced/public-shortening


### Notable public Yourls instances

+ https://your.ls (nice layout, also awesome URL)
+ https://oe.cd


### Installation guides

+ https://idroot.us/install-yourls-ubuntu-22-04 (rudimentary Apache conf)
+ https://thishosting.rocks/how-to-shorten-your-links-with-your-own-domain (July 2019)


### HTTP to HTTPS redirect (Apache config)

+ https://github.com/YOURLS/YOURLS/issues/2578
+ https://github.com/YOURLS/YOURLS/issues/2623
